/*## Завдання

Реалізувати перемикання вкладок (таби) на чистому Javascript.

#### Технічні вимоги:
- У папці `tabs` лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

#### Література:
- [Використання data-* атрибутів](https://developer.mozilla.org/ru/docs/Learn/HTML/Howto/Use_data_attributes)*/

//ВАРІАНТ 1 - лаконічний, без використання дата-атрибутів
// document.addEventListener("DOMContentLoaded", function () {
//   const tabs = document.querySelectorAll(".tabs-title");
//   const tabContents = document.querySelectorAll(".tabs-content div");

//   tabs.forEach((tab, index) => {
//     tab.addEventListener("click", () => {
//       tabs.forEach((t, i) => {
//         if (i === index) {
//           t.classList.toggle("active");
//           tabContents[i].classList.toggle("active");
//         } else {
//           t.classList.remove("active");
//           tabContents[i].classList.remove("active");
//         }
//       });
//     });
//   });
// });

//ВАРІАНТ 2 - з використанням дата-атрибутів
document.addEventListener("DOMContentLoaded", function () {
  const tabs = document.querySelectorAll(".tabs-title");
  const tabContents = document.querySelectorAll(".tabs-content div");

  tabs.forEach((tab, index) => {
    tab.addEventListener("click", () => {
      const tabName = tab.getAttribute("data-tab");
      const correspondingContent = document.querySelector(
        `[data-text="${tabName}"]`
      );

      tabs.forEach((t, i) => {
        if (i === index) {
          t.classList.toggle("active");
          correspondingContent.classList.toggle("active");
        } else {
          t.classList.remove("active");
          tabContents[i].classList.remove("active");
        }
      });
    });
  });
});
